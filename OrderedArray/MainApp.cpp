//main app for testing orderred array

#define _CRT_SECURE_NO_WARNINGS //for fopen to work

#define _CRTDBG_MAP_ALLOC //for memory leaks
#include<stdlib.h>
#include<crtdbg.h>
#include<vector>

#include"Player.h"
#include"Timer.h"
#include"MathFunctionality.h"
#include"MenuOptions.h"
//#include"MenuOptionsWithMalloc.h"

using std::cin;		using std::cout;
using std::endl;	using std::string;


int main()
{
#if defined(DEBUG)|defined(_DEBUG)
	cout << "Getting debug mode" << endl;
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif
	Timer myTimer;
	lint myMs = myTimer.GetMS();



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//uncomment to test the orderred array with player class with overloaded <= operator with players
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/*OrderedArray<Player> playerArr;

	Player p1("sebastian", 100);
	Player p2("luaks", 100);
	Player p3("ivan", 100);
	Player p4("sam", 100);
	Player p5("karol", 100);
	Player p6("anna", 100);

	playerArr.push(p1);
	playerArr.push(p2);
	playerArr.push(p3);
	playerArr.push(p4);
	playerArr.push(p5);
	playerArr.push(p6);

	for (int i = 0; i != playerArr.Size(); i++)
	{
		playerArr[i].printPlayer();
	}
*/


	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//main menu with all the options to work with integers and strings
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	


	OrderedArray<int> ordArrayInt(1,1); //set the arrays to work with them
	OrderedArray<string> strOrdArray;

	ordArrayInt.push(2);
	ordArrayInt.push(5);
	ordArrayInt.push(8);
	ordArrayInt.push(3);
	ordArrayInt.push(44);
	ordArrayInt.push(50);
	
	strOrdArray.push("anna");
	strOrdArray.push("stevo");
	strOrdArray.push("barbora");
	strOrdArray.push("samuel");
	strOrdArray.push("lukas");
	strOrdArray.push("xena");

	bool done = false;
	while (done==false)
	{
		done = printMainMenu(ordArrayInt, strOrdArray);
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//uncomment to test read and write the file but when you read it wont work with size because when you read you are not incrementing m_numOfElements 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	



	//OrderedArray<int> ordArrayInt(10,10); //set the arrays to work with them
	//OrderedArray<string> strOrdArray(10,10);

	//ordArrayInt.push(2);
	//ordArrayInt.push(5);
	//ordArrayInt.push(8);
	//ordArrayInt.push(3);
	//ordArrayInt.push(44);
	//ordArrayInt.push(50);

	//

	//strOrdArray.push("anna");
	//strOrdArray.push("stevo");
	//strOrdArray.push("barbora");
	//strOrdArray.push("samuel");
	//strOrdArray.push("lukas");
	//strOrdArray.push("xena");


	//ordArrayInt.WriteFile("writtenFileInt.bin");
	//strOrdArray.WriteFile("writtenFileStr.bin");

	//ordArrayInt.ReadFile("writtenFileInt.bin");
	//strOrdArray.ReadFile("writtenFileStr.bin");


	//ordArrayInt.printInLoopSize();
	//strOrdArray.printInLoopSize();



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//uncomment to see the speed diference betveen iterator and for loop
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//http://stackoverflow.com/questions/2524233/speed-accessing-a-stdvector-by-iterator-vs-by-operator-index speed optimozation iterator vs for loop no speed diference

	//OrderedArray<int> ordArrayInt; //set the arrays to work with them
	//for (int i = 0; i < 100000; i++)
	//{
	//	ordArrayInt.push(i);
	//}


	//myTimer.Reset();
	//for (OrderedArray<int>::iterator it = ordArrayInt.begin(); it != ordArrayInt.end(); it++)
	//{
	//	//cout << it << endl;
	//}
	//cout << "Time for iterator " << myTimer.GetMS() << endl;



	//myTimer.Reset();
	//for (int i = 0; i < ordArrayInt.Size(); i++)
	//{
	//	//cout << ordArrayInt[i] << endl;
	//}
	//cout << "Time for int loop " << myTimer.GetMS() << endl;



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	//uncomment to test with malloc when you try string it will break the code
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//MallocOrderredArray<int> malOrdArrayInt; //set the arrays to work with them
	//MallocOrderredArray<string> malStrOrdArray;

	//malOrdArrayInt.push(2);
	//malOrdArrayInt.push(5);
	//malOrdArrayInt.push(8);
	//malOrdArrayInt.push(3);
	//malOrdArrayInt.push(44);
	//malOrdArrayInt.push(50);

	//bool done = false;
	//while (done == false)
	//{
	//	done = printMainMenu(malOrdArrayInt, malStrOrdArray);
	//}

	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//unclomment to test the speed of the realoc vs new but malloc work only with primitive dataypes otherwise you need to call new 
	//before working with it not worth the speed of resize since you will call it only few times explained better in the pages bellow 
	//http://stackoverflow.com/questions/21467007/c-calloc-a-string-array  //http://stackoverflow.com/questions/3411815/how-to-use-a-c-string-in-a-structure-when-malloc-ing-the-same-structure
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//OrderedArray<int> ordArrIntTime(10,2);
	//myTimer.Reset();
	//for (int i = 0; i < 100000; i++)
	//{
	//	ordArrIntTime.push(i);
	//}	
	//cout << "Time for new resize " << myTimer.GetMS() <<" in ms "<< myTimer.GetS()<<" in sec"<< endl;
	////the time it takes to call resize of 2 in 100000 is about 24925ms about 24sec

	//MallocOrderredArray<int> mallocArrIntTime(10,2);
	//myTimer.Reset();
	//for (int i = 0; i < 100000; i++)
	//{
	//	mallocArrIntTime.push(i);
	//}
	//
	//cout << "Time for realoc resize" << myTimer.GetMS() << " in ms " << myTimer.GetS() << " in sec" <<  endl;
	////the time it takes to call realloc of 2  in 100000 is about 1578ms about 1.5sec
	////the call to realoc is about 15 times faster




	cin.ignore(2);
	return 0;
}