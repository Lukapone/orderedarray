#ifndef MALLOCMallocOrderredArray_H
#define MALLOCMallocOrderredArray_H

#include<stdlib.h>
#include<iostream>
#include<fstream>

//this is template class array that will sort the elements as they are added to it
template <class Datatype>
class MallocOrderredArray
{
private:
	Datatype* m_MallocOrderredArray;
	size_t m_size;
	int m_growSize;
	int m_numElements;

public:
	MallocOrderredArray() :m_MallocOrderredArray((Datatype*)malloc(10*sizeof(Datatype))), m_size(10), m_numElements(0), m_growSize(10)
	{
	}

	//constructor with use of initializer list
	MallocOrderredArray(int p_size, int p_growSize) :m_MallocOrderredArray((Datatype*)malloc(p_size*sizeof(Datatype))), m_size(p_size), m_numElements(0), m_growSize(p_growSize)
	{
	}
	//deconstructor automatically deletes the array when is not in use anymore
	~MallocOrderredArray()
	{
		if (m_MallocOrderredArray != 0)
			free(m_MallocOrderredArray);
		m_MallocOrderredArray = 0;
	}

	//change the growsize when needet
	void setGrowSize(int p_growSize)
	{
		if (p_growSize <= 0)
		{
			cout << "Grow size have to be bigger than 0" << endl;
		}
		else
		{
			m_growSize = p_growSize;
		}

	}



	//call this whe the array is full and you need to add the growSize to it
	void autoResize()
	{
		this->resize(m_size + m_growSize);
	}

    
	//the parameter to resize tells the size of the new array
	void resize(int p_size)
	{
		//std::cout << "calling resize" << std::endl;

		Datatype *temp = m_MallocOrderredArray;

		m_MallocOrderredArray = (Datatype*)realloc(m_MallocOrderredArray, p_size * sizeof(Datatype));

		if (m_MallocOrderredArray == 0)
		{
			m_MallocOrderredArray = temp;
			cout << "Error with realoc" << endl;
		}
		//set the size of the new array
		m_size = p_size;
	
	}

	//override [] so that it works as you would expect with an array
	Datatype& operator[](int p_index)
	{
		return m_MallocOrderredArray[p_index];
	}

	//conversion operator to convert an array into operator
	//so that we can pass our array into function
	operator Datatype*()
	{
		return m_MallocOrderredArray;
	}

	//getter to the allocated size of the array
	int allocatedSize()
	{
		return m_size;
	}

	//getter to the actual size of the array
	int Size()
	{
		return m_numElements;
	}

	//push function that will find position in sorted array
	void push(Datatype p_item)
	{
		if (m_numElements == 0)
		{
			m_MallocOrderredArray[0] = p_item;
			//after adding element increase 
			m_numElements++;
		}
		else if (m_numElements != m_size)//check if need to resize the array
		{
			for (int i = m_numElements; i > 0; i--)
			{

				if (m_MallocOrderredArray[i - 1] <= p_item)//find where to insert the item
				{
					m_MallocOrderredArray[i] = p_item;//add it to infront of the smaller
					break; //break after you insert element 
				}
				else
				{
					m_MallocOrderredArray[i] = m_MallocOrderredArray[i - 1]; //make space for another one
					if (i == 1)//if i got to last one and none element was bigger the first is the smallest
					{
						m_MallocOrderredArray[i - 1] = p_item;
					}
				}

			}
			m_numElements++;
		}
		else //resize the array
		{

			this->autoResize(); //automatically resize the array + grow value and call push again
			this->push(p_item);
		}

	}


	//depends on which size are you passing to loop through the array
	//function which will take the last element off the array
	//this pop last has complexity of constant order O(c)
	void popLast()
	{
		if (m_numElements == 0)
		{
			std::cout << "There are no elements in array to pop" << std::endl;

		}
		else
		{
			m_numElements--;//make the size smaller
		}

	}

	//remove - this remove function keeps the order of the array 
	//if order is not important there are better options
	//remove that cares about order has complexity O(n)
	void remove(int p_index)
	{

		if (0 <= p_index && p_index < m_size)
		{
			int index;
			//move everithing after index down one
			for (index = p_index + 1; index < m_size; index++)
			{
				m_MallocOrderredArray[index - 1] = m_MallocOrderredArray[index];
			}
			m_numElements--;//make the size smaller
		}
		else
		{
			std::cout << "Sorry index you used is out of bounds of this array" << std::endl;
		}


	}

	//this is simple linear search function which will loop through the array and compare the elements
	//if the element was not founfd it will return -1
	//complexity is order O(n)
	int searchLinearFromBegin(Datatype p_element)
	{
		for (int i = 0; i != m_numElements; i++)
		{
			if (m_MallocOrderredArray[i] == p_element)
			{
				return i;
			}
		}
		std::cout << "The element you were searching for was not found returning -1" << std::endl;
		return -1;
	}

	//if we know the element is closer to the end we can reverse order of binary search
	int searchLinearFromEnd(Datatype p_element)
	{
		for (int i = m_numElements - 1; i != -1; i--)
		{
			if (m_MallocOrderredArray[i] == p_element)
			{
				return i;
			}
		}
		std::cout << "The element you were searching for was not found returning -1" << std::endl;
		return -1;
	}

	//http://www.dailyfreecode.com/code/search-element-array-binary-search-1001.aspx 
	//complexity of this search is O(log2n)
	int searchBinary(Datatype p_element)
	{
		int start = 0;
		int end = m_numElements;
		int middle;
		int position = -1;

		middle = (start + end) / 2; //get the midpoint of the elements
		do
		{
			if (p_element<m_MallocOrderredArray[middle]) //if the element is in lower half change the end
			{
				end = middle - 1;
			}
			else if (p_element>m_MallocOrderredArray[middle])//if the element is in higher half change the beginning
			{
				start = middle + 1;
			}


			middle = (start + end) / 2;
		} while (start <= end && m_MallocOrderredArray[middle] != p_element);

		if (m_MallocOrderredArray[middle] == p_element)
		{
			position = middle;
		}
		else
		{
			std::cout << "The element you were searching for was not found returning -1" << std::endl;
		}


		return position;

	}


	void resetElements()//this just sets the numElements to 0
	{
		m_numElements = 0;
	}


	//simple loop and clear
	//sets all the elements to what the user want
	void linearClear(Datatype toSet)
	{
		for (int i = 0; i != m_size; i++)
		{
			m_MallocOrderredArray[i] = toSet;
		}
	}


	//write an array to file
	bool WriteFile(const char* p_filename)
	{
		FILE* outfile = 0;
		int written = 0;

		//open the file
		outfile = fopen(p_filename, "wb");  //wb means write binary

		//return if we couldnt open the file
		if (outfile == 0)
		{
			return false;
		}

		//write the array and close the file
		written = fwrite(m_MallocOrderredArray, sizeof(Datatype), m_numElements, outfile);
		fclose(outfile);

		//if we didnt write the number of elements we expected return false
		if (written != m_numElements)
		{
			return false;
		}

		return true;

	}


	//ReadFile should read an array from file
	bool ReadFile(const char* p_filename)
	{
		FILE* infile = 0;
		int read = 0;
		//open the file
		infile = fopen(p_filename, "rb");  //rb means read binary
		//if file could not be open return
		if (infile == 0)
		{
			std::cout << "File could not be open" << std::endl;
			return false;
		}

		//read the array and close file
		read = fread(m_MallocOrderredArray, sizeof(Datatype), m_size, infile);
		fclose(infile);

		//if we didnt read the number of elements we expected return false
		if (read != m_size)
		{
			return false;
		}

		return true;

	}


	//get the file check if it can be loaded and pass to the next function
	void readUnordered(std::string fileName)
	{
		std::ifstream inFile(fileName);
		if (inFile.is_open())
		{
			this->loadArray(inFile);
		}
		else
		{
			std::cout << "Loading filed" << std::endl;
		}
		inFile.close();
	}

	//simple load function which will populate array using push function
	void loadArray(std::ifstream& inFile)
	{
		Datatype element;

		while (!inFile.eof())
		{
			inFile >> element;

			this->push(element);

		}

	}

	//get the name of the file and check if you can save it than pass to next function
	void writeOrderred(std::string fileName)
	{

		std::ofstream oFile(fileName);
		if (oFile)
		{
			this->saveArray(oFile);
		}
		else
		{
			cout << "saving failed" << endl;
		}
		oFile.close();

	}

	//simple write function which will write array in order into file
	void saveArray(std::ofstream& oFile)
	{
		for (int i = 0; i != m_numElements; i++)
		{
			oFile << m_MallocOrderredArray[i] << endl;
		}
	}


	//prints all elements in array
	void printInLoopElements()
	{
		for (int i = 0; i != m_numElements; i++)
		{
			std::cout << m_MallocOrderredArray[i] << endl;
		}
	}

	//prints all elements in array and also unused allocated memory blocks
	void printInLoopSize()
	{
		for (int i = 0; i != m_size; i++)
		{
			std::cout << m_MallocOrderredArray[i] << endl;
		}
	}





};













#endif