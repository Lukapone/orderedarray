#ifndef MENUOPTIONS_H
#define MENUOPTIONS_H
//navigational menu for the inputs from user

#include<string>
#include"OrderedArray.h"

//menu functions and functions for working with array
bool printMainMenu(OrderedArray<int>& p_intArray, OrderedArray<std::string>& p_floatArray);
bool getInputMainMenu(OrderedArray<int>& p_intArray, OrderedArray<std::string>& p_floatArray);

bool printCreationMenu(OrderedArray<int>& p_intArray);
bool getInputCreationMenu(OrderedArray<int>& p_intArray);

bool printCreationMenuStr(OrderedArray<std::string>& p_strArray);
bool getInputCreationMenuStr(OrderedArray<std::string>& p_strArray);




#endif