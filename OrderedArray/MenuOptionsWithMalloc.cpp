//this file contains functions that prints options for user and take his input

#include"MenuOptionsWithMalloc.h"
#include"Timer.h"
#include"UserInput.h"
typedef long long int lint;



using std::cin;		using std::cout;
using std::endl;	using std::string;


bool printMainMenu(MallocOrderredArray<int>& p_intArray, MallocOrderredArray<string>& p_strArray)
{
	cout << "Menu: " << endl;
	cout << "Press 1 to perform actions on array of integers" << endl;
	cout << "      2 to perform actions on array of strings" << endl;
	cout << "      3 to load array from text file " << endl;
	cout << "      4 to save an array into text file " << endl;
	cout << "      5 to exit" << endl;
	return getInputMainMenu(p_intArray, p_strArray);

}

//function which is manipulating array according user input
bool getInputMainMenu(MallocOrderredArray<int>& p_intArray, MallocOrderredArray<string>& p_strArray)
{
	int userChoice = userInput();

	if (userChoice == 1)
	{
		printCreationMenu(p_intArray);
	}
	else if (userChoice == 2)
	{
		printCreationMenuStr(p_strArray);
	}
	else if (userChoice == 3)
	{
		//comment or uncomment which you want to read
		//p_intArray.resetElements();//this clears elements to 0 so the file will be loaded into empty array
		//p_intArray.readUnordered(userInputStrWithText("Enter file name of file to be loaded"));

		p_strArray.resetElements();//this clears elements to 0 so the file will be loaded into empty array
		p_strArray.readUnordered(userInputStrWithText("Enter file name of file to be loaded"));
	}
	else if (userChoice == 4)
	{
		//comment or uncomment which you want to write
		//p_intArray.writeOrderred(userInputStrWithText("Enter file name of file to be saved"));
		p_strArray.writeOrderred(userInputStrWithText("Enter file name of file to be saved"));
	}
	else if (userChoice == 5)
	{
		return true;
	}
	else
	{
		cout << "Invalid input" << endl;
		cin.clear();//clears the buffer
		cin.ignore(80, '\n'); //ignores characters until end of line
	}

	return false;
}
//           INTEGERS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool printCreationMenu(MallocOrderredArray<int>& p_intArray)
{
	cout << "Default setting are 10 for array size and growsize is 10 too" << endl;
	cout << "Press 1 to change size" << endl;
	cout << "      2 to change grow size" << endl;
	cout << "      3 to push element into array" << endl;
	cout << "      4 to pop the last element off" << endl;
	cout << "      5 to remove element of an index" << endl;
	cout << "      6 to use binary search" << endl;
	cout << "      7 to use linear search" << endl;
	cout << "      8 to clear array (set elements to user walues)" << endl;
	cout << "      9 to print array" << endl;
	cout << "      10 to exit" << endl;

	return getInputCreationMenu(p_intArray);
}


bool getInputCreationMenu(MallocOrderredArray<int>& p_intArray)
{
	Timer myTimer;
	lint myMs = myTimer.GetMS();
	int userChoice = userInput();

	if (userChoice == 1)
	{
		p_intArray.resize(userInputWithText("Enter new size for array"));
	}
	else if (userChoice == 2)
	{
		p_intArray.setGrowSize(userInputWithText("Enter new grow size"));
	}
	else if (userChoice == 3)
	{
		p_intArray.push(userInputWithText("Enter element to push"));
	}
	else if (userChoice == 4)
	{
		p_intArray.popLast();
	}
	else if (userChoice == 5)
	{
		p_intArray.remove(userInputWithText("Enter index of element to be removed"));
	}
	else if (userChoice == 6)
	{

		int toSearch = userInputWithText("Enter element for binary search");
		myTimer.Reset();
		int index = p_intArray.searchBinary(toSearch);
		cout << "The index of you binary rsearch is " << index << endl;
		cout << "Time to do binary search was " << myTimer.GetMS() << " ms" << endl;
	}
	else if (userChoice == 7)
	{

		int toSearch = userInputWithText("Enter element for linear search");
		myTimer.Reset();
		int index = p_intArray.searchLinearFromBegin(toSearch);
		cout << "The index of you linear rsearch is " << index << endl;
		cout << "My time in ms for linear search " << myTimer.GetMS() << endl;
	}
	else if (userChoice == 8)
	{
		p_intArray.linearClear(userInputWithText("Enter the walue you want the elements to be set to"));
	}
	else if (userChoice == 9)
	{
		cout << "Printing elements" << endl;
		p_intArray.printInLoopElements();
		cout << "Printing elements and allocated memory blocks" << endl;
		p_intArray.printInLoopSize();
	}
	else if (userChoice == 10)
	{
		return true;
	}
	else
	{

		cout << "Invalid input" << endl;
		cin.clear();//clears the buffer
		cin.ignore(80, '\n'); //ignores characters until end of line
	}
	return false;
}

//         STRINGS
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



bool printCreationMenuStr(MallocOrderredArray<string>& p_strArray)
{
	cout << "Default setting are 10 for array size and growsize is 10 too" << endl;
	cout << "Press 1 to change size" << endl;
	cout << "      2 to change grow size" << endl;
	cout << "      3 to push element into array" << endl;
	cout << "      4 to pop the last element off" << endl;
	cout << "      5 to remove element of an index" << endl;
	cout << "      6 to use binary search" << endl;
	cout << "      7 to use linear search" << endl;
	cout << "      8 to clear array (set elements to user walues)" << endl;
	cout << "      9 to print array" << endl;
	cout << "      10 to exit" << endl;

	return getInputCreationMenuStr(p_strArray);
}


bool getInputCreationMenuStr(MallocOrderredArray<string>& p_strArray)
{
	Timer myTimer;
	lint myMs = myTimer.GetMS();
	int userChoice = userInput();

	if (userChoice == 1)
	{
		p_strArray.resize(userInputWithText("Enter new size for array"));
	}
	else if (userChoice == 2)
	{
		p_strArray.setGrowSize(userInputWithText("Enter new grow size"));
	}
	else if (userChoice == 3)
	{
		p_strArray.push(userInputStrWithText("Enter element to push"));
	}
	else if (userChoice == 4)
	{
		p_strArray.popLast();
	}
	else if (userChoice == 5)
	{
		p_strArray.remove(userInputWithText("Enter index of element to be removed"));
	}
	else if (userChoice == 6)
	{

		string toSearch = userInputStrWithText("Enter element for binary search");
		myTimer.Reset();
		int index = p_strArray.searchBinary(toSearch);
		cout << "The index of you binary rsearch is " << index << endl;
		cout << "Time to do binary search was " << myTimer.GetMS() << " ms" << endl;
	}
	else if (userChoice == 7)
	{

		string toSearch = userInputStrWithText("Enter element for linear search");
		myTimer.Reset();
		int index = p_strArray.searchLinearFromBegin(toSearch);
		cout << "The index of you linear rsearch is " << index << endl;
		cout << "My time in ms for linear search " << myTimer.GetMS() << endl;
	}
	else if (userChoice == 8)
	{
		p_strArray.linearClear(userInputStrWithText("Enter the walue you want the elements to be set to"));
	}
	else if (userChoice == 9)
	{
		cout << "Printing elements" << endl;
		p_strArray.printInLoopElements();
		cout << "Printing elements and allocated memory blocks" << endl;
		p_strArray.printInLoopSize();
	}
	else if (userChoice == 10)
	{
		return true;
	}
	else
	{

		cout << "Invalid input" << endl;
		cin.clear();//clears the buffer
		cin.ignore(80, '\n'); //ignores characters until end of line
	}
	return false;
}

