#ifndef MENUOPTIONSWITHMALLOC_H
#define MENUOPTIONSWITHMALLOC_H
//navigational menu for the inputs from user

#include<string>
#include"MallocOrderredArray.h"

//menu functions and functions for working with array
bool printMainMenu(MallocOrderredArray<int>& p_intArray, MallocOrderredArray<std::string>& p_floatArray);
bool getInputMainMenu(MallocOrderredArray<int>& p_intArray, MallocOrderredArray<std::string>& p_floatArray);

bool printCreationMenu(MallocOrderredArray<int>& p_intArray);
bool getInputCreationMenu(MallocOrderredArray<int>& p_intArray);

bool printCreationMenuStr(MallocOrderredArray<std::string>& p_strArray);
bool getInputCreationMenuStr(MallocOrderredArray<std::string>& p_strArray);



#endif