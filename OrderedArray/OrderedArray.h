#ifndef ORDEREDARRAY_H
#define ORDEREDARRAY_H


#include<iostream>
#include<fstream>

//usefull implementations http://stackoverflow.com/questions/5159061/implementation-of-vector-in-c
//this is template class array that will sort the elements as they are added to it
template <class Datatype>
class OrderedArray
{
private:
	//default constant values if you change here all uses are changed
	static const int DEFAULT_SIZE = 10;
	static const int DEFAULT_GROW_SIZE = 10;
	Datatype* m_OrderedArray=0;
	size_t m_size;
	int m_growSize;
	int m_numElements;

public:
	//default constructor we could also use m_size/2 for growsize as is used in normal vector, but since is user defined it does not matter
	OrderedArray() :m_OrderedArray(new Datatype[DEFAULT_SIZE]), m_size(DEFAULT_SIZE), m_numElements(0), m_growSize(DEFAULT_GROW_SIZE)
	{
		//check if you were able to construct object
		if (m_OrderedArray == 0)
		{
			std::cout << "The call to new failed and returned 0" << std::endl;
			return;
		}
	}
	
	//constructor with use of initializer list
	OrderedArray(int p_size,int p_growSize) :m_OrderedArray(new Datatype[p_size]), m_size(p_size), m_numElements(0), m_growSize(p_growSize)
	{
		//check if you were able to construct object
		if (m_OrderedArray == 0)
		{
			std::cout << "The call to new failed and returned 0" << std::endl;
			return;
		}
	}

	//deconstructor automatically deletes the array when is not in use anymore
	~OrderedArray()
	{
		if (m_OrderedArray != 0)
			delete[]m_OrderedArray;
		m_OrderedArray = 0;
	}

	//change the growsize when needet
	void setGrowSize(int p_growSize)
	{
		if (p_growSize <= 0)
		{
			cout << "Grow size have to be bigger than 0" << endl;
		}
		else
		{
			m_growSize = p_growSize;
		}
		
	}



	//call this whe the array is full and you need to add the growSize to it
	void autoResize()
	{
		this->resize(m_size + m_growSize);
	}

		//the parameter to resize tells the size of the new array
	void resize(int p_size)
	{
		//std::cout << "calling resize" << std::endl;
		//step one: Create new array
		Datatype* newArray = 0;
		
			newArray = new Datatype[p_size];
	
		//if the new array wasn't allocated, then just return
		//when it fails return 0 or bad_alloc exception
		if (newArray == 0)
		{
			std::cout << "The call to new failed and returned 0" << std::endl;
			return;
		}

		//determine if i am making the new array bigger or smaller
		int min;
		if (p_size < m_size)
		{
			min = p_size;
		}
		else
		{
			min = m_size;
		}

		//loop through and copy all elements
		for (int index = 0; index < min; index++)
		{
			newArray[index] = m_OrderedArray[index];
		}

		//set the size of the new array
		m_size = p_size;

		//delete the old array
		if (m_OrderedArray != 0)
		{
			//make sure you use delete[] not just delete
			delete[]m_OrderedArray;
		}

		//copy the pointer over
		m_OrderedArray = newArray;
		newArray = 0;

	}

	//override [] so that it works as you would expect with an array
	Datatype& operator[](int p_index)
	{
		return m_OrderedArray[p_index];
	}

	//conversion operator to convert an array into operator
	//so that we can pass our array into function
	operator Datatype*()
	{
		return m_OrderedArray;
	}

	//getter to the allocated size of the array
	int allocatedSize()
	{
		return m_size;
	}

	//getter to the actual size of the array
	int Size()
	{
		return m_numElements;
	}

	//push function that will find position in sorted array
	//this function has order O(n) if you want to push the element to the beginning and O(c) if you would push the elements only to the end
	//so the average order will be O(n/2)
	void push(Datatype p_item)
	{
		if (m_numElements == 0)
		{
			m_OrderedArray[0] = p_item;
			//after adding element increase 
			m_numElements++;
		}
		else if (m_numElements!=m_size)//check if need to resize the array
		{
				for (int i = m_numElements; i > 0; i--)
				{

					if (m_OrderedArray[i - 1] <= p_item)//find where to insert the item
					{
						m_OrderedArray[i] = p_item;//add it to infront of the smaller
						break; //break after you insert element 
					}
					else
					{
						m_OrderedArray[i] = m_OrderedArray[i - 1]; //make space for another one
						if (i == 1)//if i got to last one and none element was bigger the first is the smallest
						{
							m_OrderedArray[i-1] = p_item;
						}
					}
					
				}
				m_numElements++;
		}
		else //resize the array
		{

			this->autoResize(); //automatically resize the array + grow value and call push again
			this->push(p_item);
		}
		
	}


		//depends on which size are you passing to loop through the array
		//function which will take the last element off the array
		//this pop last has complexity of constant order O(c)
		void popLast()
		{
			if (m_numElements == 0)
			{
				std::cout << "There are no elements in array to pop" << std::endl;
				
			}
			else
			{
				m_numElements--;//make the size smaller
			}
			
		}

		//remove - this remove function keeps the order of the array 
		//if order is not important there are better options
		//remove that cares about order has complexity O(n)
		void remove(int p_index)
		{
			if (m_numElements == 0)
			{
				std::cout << "Array is empty nothing to remove" << std::endl;
				return;
			}
			if (0 <= p_index && p_index < m_size)
			{
					int index;
				//move everithing after index down one
				for (index = p_index + 1; index < m_size; index++)
				{
					m_OrderedArray[index - 1] = m_OrderedArray[index];
				}
				m_numElements--;//make the size smaller
			}
			else
			{
				std::cout << "Sorry index you used is out of bounds of this array" << std::endl;
			}

			
		}

		//this is simple linear search function which will loop through the array and compare the elements
		//if the element was not founfd it will return -1
		//complexity is order O(n)
		int searchLinearFromBegin(Datatype p_element)
		{
			for (int i = 0; i != m_numElements; i++)
			{
				if (m_OrderedArray[i] == p_element)
				{
					return i;
				}
			}
			std::cout << "The element you were searching for was not found returning -1" << std::endl;
			return -1;
		}

		//if we know the element is closer to the end we can reverse order of binary search
		int searchLinearFromEnd(Datatype p_element)
		{
			for (int i = m_numElements-1; i != -1; i--)
			{
				if (m_OrderedArray[i] == p_element)
				{
					return i;
				}
			}
			std::cout << "The element you were searching for was not found returning -1" << std::endl;
			return -1;
		}


		//we can also use one loop to go from both sides of the array 
		//the order of the search is O(n)
		int searchLinearBothEnds(Datatype p_element)
		{
			for (int i = 0, j = m_numElements - 1; i < (m_numElements+1)/2 ; i++,j--)
			{
				std::cout << i << j << std::endl;
				if (m_OrderedArray[i] == p_element)
				{
					return i;
				}
				if (m_OrderedArray[j] == p_element)
				{
					return j;
				}

			}
			std::cout << "The element you were searching for was not found returning -1" << std::endl;
			return -1;
		}




		//http://www.dailyfreecode.com/code/search-element-array-binary-search-1001.aspx 
		//complexity of this search is O(log2n)
		int searchBinary(Datatype p_element)
		{
			int start = 0;
			int end = m_numElements;
			int middle;
			int position = -1;

				middle = (start + end) / 2; //get the midpoint of the elements
			do
			{
				if (p_element<m_OrderedArray[middle]) //if the element is in lower half change the end
				{
					end = middle - 1;
				}
				else if(p_element>m_OrderedArray[middle])//if the element is in higher half change the beginning
				{
					start = middle + 1;
				}
					

				middle = (start + end) / 2;
			} while (start <= end && m_OrderedArray[middle] != p_element);

			if (m_OrderedArray[middle] == p_element)
			{
				position = middle;
			}
			else
			{
				std::cout << "The element you were searching for was not found returning -1" << std::endl;
			}
				

			return position;
			
		}


		void resetElements()//this just sets the numElements to 0
		{
			m_numElements = 0;
		}


		//simple loop and clear
		//sets all the elements to what the user want
		//order of the clean is O(n)
		void linearClear(Datatype toSet)
		{
			for (int i = 0; i != m_size; i++)
			{
				m_OrderedArray[i] = toSet;
			}
		}


		//write an array to file
		bool WriteFile(const char* p_filename)
		{
			FILE* outfile = 0;
			int written = 0;

			//open the file
			outfile = fopen(p_filename, "wb");  //wb means write binary

			//return if we couldnt open the file
			if (outfile == 0)
			{
				return false;
			}

			//write the array and close the file
			written = fwrite(m_OrderedArray, sizeof(Datatype), m_numElements, outfile);
			fclose(outfile);

			//if we didnt write the number of elements we expected return false
			if (written != m_numElements)
			{
				return false;
			}

			return true;

		}


		//ReadFile should read an array from file
		bool ReadFile(const char* p_filename)
		{
			std::cout << "callingreadfile" << std::endl;
			FILE* infile = 0;
			int read = 0;
			//open the file
			infile = fopen(p_filename, "rb");  //rb means read binary
			//if file could not be open return
			if (infile == 0)
			{
				std::cout << "File could not be open" << std::endl;
				return false;
			}

			//read the array and close file
			read = fread(m_OrderedArray, sizeof(Datatype), m_size, infile);
			fclose(infile);

			//if we didnt read the number of elements we expected return false
			if (read != m_size)
			{
				return false;
			}

			return true;

		}


		//get the file check if it can be loaded and pass to the next function
		void readUnordered(std::string fileName)
		{
			std::ifstream inFile(fileName);
			if (inFile.is_open())
			{
				this->loadArray(inFile);
			}
			else
			{
				std::cout << "Loading filed" << std::endl;
			}
			inFile.close();
		}

		//simple load function which will populate array using push function
		void loadArray(std::ifstream& inFile)
		{
			Datatype element;

			while (!inFile.eof())
			{
				inFile >> element;
				
				this->push(element);
				
			}

		}

		//get the name of the file and check if you can save it than pass to next function
		void writeOrderred(std::string fileName)
		{

			std::ofstream oFile(fileName);
			if (oFile)
			{
				this->saveArray(oFile);
			}
			else
			{
				cout << "saving failed" << endl;
			}
			oFile.close();

		}

		//simple write function which will write array in order into file
		void saveArray(std::ofstream& oFile)
		{
			for (int i = 0; i != m_numElements; i++)
			{
				oFile << m_OrderedArray[i] << endl;
			}
		}


		//prints all elements in array
		void printInLoopElements()
		{
			for (int i = 0; i != m_numElements; i++)
			{
				std::cout << m_OrderedArray[i] << endl;
			}
		}

		//prints all elements in array and also unused allocated memory blocks
		void printInLoopSize()
		{
			for (int i = 0; i != m_size; i++)
			{
				std::cout << m_OrderedArray[i] << endl;
			}
		}

		//custom iterator
		typedef Datatype* iterator;

		typename iterator begin()
		{
			return m_OrderedArray;
		}


		typename iterator end()
		{
			return m_OrderedArray+m_numElements;
		}

};













#endif