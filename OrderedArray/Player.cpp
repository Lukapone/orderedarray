// implementation of simple player class
#include"Player.h"
#include<iostream>

using std::string;		using std::cout;
using std::endl;


Player::Player() :m_name("noName"), m_health(100)
{
}

Player::Player(string p_name, int p_health) : m_name(p_name), m_health(p_health)
{
}

Player::~Player()
{
}

//simple print to see what is in variables
void Player::printPlayer()
{
	cout << "Your name: " << m_name << endl;
	cout << "Your health " << m_health << endl;
}

//http://courses.cms.caltech.edu/cs11/material/cpp/donnie/cpp-ops.html about operator overloading
//overloaded <= operator to enable use of my player class with my orderred array
bool Player::operator<=(const Player& other)
{
	//sorted by names we can change to sort by hp if we want
	if (this->m_name <= other.m_name)
	{
		return true;
	}
	return false;
}























