#ifndef PLAYER_H
#define PLAYER_H

#include<string>


//simple player class to test the orderred array class with overloaded <= operator

class Player
{
private:
	std::string m_name;
	int m_health;

public:
	Player();
	Player(std::string p_name,int p_health);
	~Player();
	void printPlayer();
	bool operator<=(const Player& other);


};

#endif