//implement timer.

#include "timer.h"
#include <ctime>

#ifdef _WIN32
#include <Windows.h>
#else
#include <sys/time.h>
#endif

#ifdef _WIN32
class Win32PerformanceCounter
{
public:
	lint m_freq;
	Win32PerformanceCounter()
	{
		//get the 'tick per second'
		QueryPerformanceFrequency((LARGE_INTEGER*)(&m_freq));

		//returns ticks per second but i want milliseconds
		m_freq = m_freq / 1000;
	}
};
Win32PerformanceCounter g_Win32Counter;
#endif

//===========================================================================================================
//These function get a time value.Meaning of time is relative.
//===========================================================================================================

lint GetTimeMS()
{
#ifdef _WIN32
	lint t;
	//int* myInt; casting it into pointer
	QueryPerformanceCounter((LARGE_INTEGER*)(&t));
	return t / g_Win32Counter.m_freq;
#else
	struct timeval t;
	lint s;
	gettimeofday(&t, 0);
	s = t.tv_sec;
	s *= 1000;
	//i want from microsecond to millisecond
	s += (t.tv_usec / 1000);

	return s;
#endif
}

lint GetTimeS()
{
	return GetTimeMS() / 1000;
}

lint GetTimeM()
{
	return GetTimeMS() / 60000;
}

lint GetTimeH()
{
	return GetTimeMS() / 3600000;
}

std::string TimeStamp()
{
	char str[9];

	time_t a = time(0);
	struct tm b;
	localtime_s(&b, &a);
	strftime(str, 9, "%H:%M:%S", &b);
	return str;
}

//this print datastamp in YYYY:MM:DD

std::string DateStamp()
{
	char str[11];

	time_t a = time(0);
	struct tm b;

	localtime_s(&b, &a);



	strftime(str, 11, "%Y:%m:%d", &b);
	return str;
}

//class constructor which is with initializer list. This is good always try to do this because it means that setting walues is one step process instead of 2 steps
Timer::Timer() :m_startTime(0), m_initTime(0)
{
}

void Timer::Reset(lint p_timePassed)
{
	m_startTime = p_timePassed;
	m_initTime = GetTimeMS();
}

lint Timer::GetMS()
{
	//return amount of time since the time was initialized, plus whatever starting timer had
	return(GetTimeMS() - m_initTime) + m_startTime;
}

lint Timer::GetS()
{
	return GetMS() / 1000;
}

lint Timer::GetM()
{
	return GetMS() / (60 * 1000);
}

lint Timer::GetH()
{
	return GetMS() / (60 * 60 * 1000);
}

lint Timer::GetD()
{
	return GetMS() / (24 * 60 * 60 * 1000);
}

lint Timer::GetY()
{
	//is too big for integer that why we need to put it into lint
	lint number = 365 * 24 * 60 * 60 * 1000;
	return GetMS() / number;
}

std::string Timer::GetString()
{
	std::string str;
	lint y = GetY();
	lint d = GetD() % 365;
	lint h = GetH() % 24;
	lint m = GetM() % 60;

	if (y > 0)
	{
		str += y + " years, ";
	}
	if (d > 0)
	{
		str += d + " days, ";
	}
	if (h > 0)
	{
		str += h + " hours, ";
	}

	str += m + " minutes";
	return str;

}


