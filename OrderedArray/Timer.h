#ifndef TIMER_H
#define TIMER_H
// Timer class which provide a millisecond timer,along with timer based on it.
#include<string>

typedef long long int lint;
lint GetTimeMS();
lint GetTimeS();
lint GetTimeM();
lint GetTimeH();

std::string TimeStamp();
std::string DateStamp();

class Timer
{
private:
	//this is the time at which the timer is initialized
	lint m_initTime;
	//Starting time of the timer
	lint m_startTime;

public:
	Timer();
	void Reset(lint p_timePassed = 0);
	lint GetMS();
	lint GetS();
	lint GetM();
	lint GetH();
	lint GetD();
	lint GetMonth();
	lint GetY();


	std::string GetString();

};

inline lint second(lint t){ return t * 1000; }
inline lint minute(lint t){ return t * 1000 * 60; }
inline lint hour(lint t){ return t * 1000 * 60 * 60; }
inline lint day(lint t){ return t * 1000 * 60 * 60 * 24; }
inline lint year(lint t){ return t * 1000 * 60 * 60 * 24 * 365; }


#endif