
#include"UserInput.h"
#include<iostream>
typedef long long int lint;



using std::cin;		using std::cout;
using std::endl;	using std::string;






//puts > before user input
void expInput()
{
	cout << ">";
}

//function that takes input
int userInput()
{
	int input = 0;

	//this ads > before input
	expInput();
	cin >> input;

	return input;
}

string userInputStr()
{
	string word = "";

	//this ads > before input
	expInput();
	cin >> word;

	return word;
}

//functions that takes input and displays what is expected from user
int userInputWithText(string p_text)
{
	cout << p_text << endl;
	int input = 0;

	//this ads > before input
	expInput();
	cin >> input;

	return input;
}

string userInputStrWithText(string p_text)
{
	cout << p_text << endl;
	string word = "";

	//this ads > before input
	expInput();
	cin >> word;

	return word;
}